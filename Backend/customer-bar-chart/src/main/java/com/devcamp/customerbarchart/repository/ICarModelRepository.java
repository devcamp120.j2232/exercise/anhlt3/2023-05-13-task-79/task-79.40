package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerbarchart.model.CarModel;

public interface ICarModelRepository extends JpaRepository<CarModel, Long> {
	CarModel findByCarCode(String carCode);
}

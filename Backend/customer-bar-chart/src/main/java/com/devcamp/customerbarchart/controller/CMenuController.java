package com.devcamp.customerbarchart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.customerbarchart.model.CMenu;
import com.devcamp.customerbarchart.repository.IMenuRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CMenuController {
	@Autowired
	IMenuRepository pMenuRepository;

	@GetMapping("/menu")
	public ResponseEntity<List<CMenu>> getAllMenu() {
		try {
			List<CMenu> pMenuList = new ArrayList<CMenu>();

			pMenuRepository.findAll().forEach(pMenuList::add);

			return new ResponseEntity<>(pMenuList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

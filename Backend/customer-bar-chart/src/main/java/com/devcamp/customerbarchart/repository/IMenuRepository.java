package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerbarchart.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Long> {

}
